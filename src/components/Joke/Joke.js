import React, {Component} from 'react'
import './Joke.css'

export default class Post extends Component{
    
    componentWillMount(){
        console.log('[Joke] Will Mount');
    }
    componentDidMount(){
        console.log('[Joke] Did Mount');
    }
    componentWillUpdate() {
        console.log('[Joke] Will Update');
    }
    shouldComponentUpdate(nextProps, nextState){
        console.log('[Joke] ShouldUpdate')
        return nextProps.url !== this.props.url || 

        nextProps.value !== this.props.value;
    }
    render(){
        console.log('[Joke] render')
        return(
            <div className="Joke" onClick={this.props.clicked}>
                <h1>Jokes with Chuck Norris</h1>
                
                <div className="Info">
                    <div><img src={this.props.icon_url}></img></div>
                
                    <div className="Author">{this.props.jokes}</div>
                </div>
            </div>
        )
    }
} 